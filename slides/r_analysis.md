<div class="footer" > 
  <div class="left">
    Universität Tübingen, 2020
  </div>
  <div class="right">
    David Kirschenheuter - Textanalyse mit R
  </div>
</div>


Datenanalyse mit R und der New York Times
========================================================
author: David Kirschenheuter
date: 23.03.2020
autosize: true
font-family: 'Helvetica'
css: css/league.css
width: 1600
height: 900

R - Kurzvorstellung
========================================================

- R: Programmiersprache für Statistik und mehr
  - Basiert auf S
  - Quelloffen
- RStudio: Entwicklungsumgebung für R
- R Packages: Zusatzpakete für R
- Funktionalität:
  - Daten importieren
  - Daten transformieren
  - Daten analysieren
  - Daten visualisieren
  - Daten exportieren

Arbeiten mit R und Rstudio
========================================================
Code auf der Console ausführen

```r
1+2
```

```
[1] 3
```

Code als Variable speichern: mit dem Zuweisungspfeil ->

```r
a <- 1+2
a
```

```
[1] 3
```

Variablen im Code nutzen

```r
b <- a+3
b
```

```
[1] 6
```

Arbeiten mit R und RStudio
========================================================
- Programmcode sollte als Skript gespeichert werden und von dort ausgeführt werden

- In RStudio links oben ein neues Skript anlegen

- Gesamtes Skript mit dem Run-Button ausführen

- Teilcode durch markieren und STRG+ENTER ausführen

Projekt Setup
========================================================

- Sämtliche Skripte und Präsentationen finden sich in einem Gitlab-Repository: <https://gitlab.com/davidki/nyt-textmining>.
- RStudio kann Dateien und Einstellungen in Projekten verwalten: sehr empfehlenswert
- Dazu in RStudio File->New Project... -> New Directory -> New Project 
- Speicherpfad und Projektname angeben -> Create Project
- Der Speicherpfad des Projekts ist jetzt das Homeverzeichnis zum laden und speichern von Dateien/Daten

Die Daten: NYT-API
========================================================

- Die hier verwendeten Daten stammen von <https://developers.nytimes.com/>.
- Archiv der New York Times von 1851 bis heute
- Daten werden mit dem Skript *nyt_api.R* bezogen und als *2010_2019_nyt_archive.csv* gespeichert

Fragestellung und Workflow
========================================================

**Fragestellung:**
- Unterscheiden sich die Artikel der NYT zwischen der Amtszeit von Obama und Trump in ihrer emotionalen Wirkung?

**Workflow**
- NYT-Daten importieren (01.01.2010 bis 31.12.2019)
- Daten transformieren
  - Subset/Teilmenge erstellen: ein Jahr Obama und ein Jahr Trump
  - Artikel-Snippet in einzelne Wörter trennen
  - Wörter mit einem Sentiment-Lexikon vergleichen
- Positive und Negative Wörter der Trump- und der Obamaamtszeit vergleichen
- Ergebnisse visualisieren
  

Nötige Packages (Add-Ons) laden
========================================================
- Packages installieren: Fenster rechts unten, Reiter Packages oder *install.packages()*

```r
# download and activate packages
## Package for text analysis
library(tidytext)
## package for tidy data
library(dplyr)
## package for plotting
library(ggplot2)
```


Daten laden
========================================================
- Mit *#* wird ein Kommentar eingeleitet
- read.csv ist eine R-Funktion zum laden von CSV-Dateien


```r
# load the raw data (produces by nyt_api.R)
archive <- read.csv("../data/2010_2019_nyt_archive.csv")
```
- Hilfe zu Funktionen:
  - Im Fenster rechts unten unter dem Reiter "Help"
  - oder durch `R ?read.csv`
  
Subset erstellen
========================================================
- Mit dem Pipe-Operator (*%>%*) wird das Ergebnis des vorausgehenden Befehls als Input für die folgenden Befehle genutzt
- *filter()*: Filtert Daten, Syntax: Variable == Wert
- Mit *&* werden Bedingungen addiert

```r
# create two subsets: Obama and Trump
obama <- archive %>%
  filter(as.Date(pub_date) >= "2015-01-01" & as.Date(pub_date) <= "2015-12-31")

trump <- archive %>%
  filter(as.Date(pub_date) >= "2018-01-01" & as.Date(pub_date) <= "2018-12-31")
```

|      X|headline                                             |pub_date   |pub_time |snippet                                                                                                            |
|------:|:----------------------------------------------------|:----------|:--------|:------------------------------------------------------------------------------------------------------------------|
| 641130|The New Year Around the World                        |2015-01-01 |00:00    |Celebrations, with light shows, fireworks and even a camel ride, ushered in 2015.                                  |
| 641131|A Guide to Minimum Wage Increases at the State Level |2015-01-01 |01:45    |By Jan. 1, 29 states and the District of Columbia will have minimum wages above the federal minimum wage of $7.25. |


Daten in einzelne Wörter zerlegen
========================================================
- *tibble()*: Strukturiert Daten in einer Tabelle. Syntax: Spaltennanem = Spalteninhalt
- *unnest_tokens*: zerlegt Text in einzelne Wörter

```r
#unnest the data
snippet_obama <- tibble(number = 1:nrow(obama), snippet = as.character(obama$snippet), pub_date = obama$pub_date) %>%
  unnest_tokens(word, snippet)

snippet_trump <- tibble(number = 1:nrow(trump), snippet = as.character(trump$snippet), pub_date = trump$pub_date) %>%
  unnest_tokens(word, snippet)
```

| number|pub_date   |word         |
|------:|:----------|:------------|
|      1|2015-01-01 |celebrations |
|      1|2015-01-01 |with         |
|      1|2015-01-01 |light        |
|      1|2015-01-01 |shows        |

Daten in einzelne Wörter zerlegen
========================================================
- *anti_join*: Verknüpft zwei Tabellen und behält nur Inhalte der ersten Tabelle, die nicht in der zweiten vorkommen

```r
# remove stop words
## stop words from tidy text
data(stop_words)
## perform anti join to keep only words, which are not present in stop_words
snippet_obama <- snippet_obama %>%
  anti_join(stop_words)

snippet_trump <- snippet_trump %>%
  anti_join(stop_words)
```

Wörter mit einem Sentiment-Lexikon abgleichen
========================================================
- *get_sentiment("bing")*: erstellt ein Lexikon mit Wörtern und deren emotionaler Wirkung (sentiment)
- *inner_join()*: verknüpft zwei Tabellen und behällt Inhalte die in beiden Tabellen vorkommen

```r
# calculate the sentiments using bing dictionary (https://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html)
bing <- get_sentiments("bing")

obama_sentiment <- inner_join(snippet_obama, bing)

trump_sentiment <- inner_join(snippet_trump, bing)
```


| number|pub_date   |word      |sentiment |
|------:|:----------|:---------|:---------|
|      3|2015-01-01 |ready     |positive  |
|      3|2015-01-01 |cheating  |negative  |
|      6|2015-01-02 |led       |positive  |
|      7|2015-01-02 |wonderful |positive  |


Positive und Negative Wörter auszählen
========================================================
- *group_by*: ordnet Werte einer Zeile einer Gruppe von Werten zu
- *summarise*: fasst Werte zusammen, z.B. durch auszählen mit *n()*
- *mutate*: fügt Spalten einer Tabelle hinzu
- Wieviele negative und positive Wörter haben die beiden Datensätze?


```r
#count number of pos and neg
counts <- bind_rows(
  obama_sentiment %>%
    group_by(sentiment) %>%
    summarise(absolute = n()) %>%
    mutate(relative = absolute/sum(absolute)*100,
           period = "obama"),
  trump_sentiment %>%
    group_by(sentiment) %>%
    summarise(absolute = n()) %>%
    mutate(relative = absolute/sum(absolute)*100,
           period = "trump")
)
```

Positive und Negative Wörter auszählen
========================================================

|sentiment | absolute| relative|period |
|:---------|--------:|--------:|:------|
|negative  |    61757| 52.17197|obama  |
|positive  |    56615| 47.82803|obama  |
|negative  |    55280| 56.88179|trump  |
|positive  |    41904| 43.11821|trump  |
Und jetzt bitte als anschauliche Grafik!
- Viualisierung mit dem Paket **ggplot2**

Positive und Negative Wörter auszählen
========================================================


```r
## counting pos and neg
ggplot() +
  geom_bar(data = counts,
           aes(x=period, y= relative, fill = sentiment),
           stat="identity", position = position_dodge(0.9)) +
  scale_fill_manual(
    values = c(
      negative = "#c90000",
      positive = "#268302"
    )
  ) +
  geom_text(data = counts,
            aes(x=period, y=relative, fill = sentiment, label=paste(round(relative,1),"%")),
            stat="identity", position = position_dodge(0.9), vjust=-1)
```
***
![plot of chunk unnamed-chunk-16](r_analysis-figure/unnamed-chunk-16-1.png)

Die häufigsten Wörter der beiden Datensets vergleichen
========================================================

```r
com_words <- bind_rows(obama_sentiment %>%
  count(word, sentiment, sort = TRUE) %>%
    ungroup() %>%
    group_by(sentiment) %>%
    top_n(10) %>%
    mutate(period = "obama"),
  trump_sentiment %>%
  count(word, sentiment, sort = TRUE) %>%
    ungroup() %>%
    group_by(sentiment) %>%
    top_n(10) %>%
    mutate(period = "trump")
)
```

Die häufigsten Wörter der beiden Datensets vergleichen
========================================================

|word       |sentiment |    n|period |
|:----------|:---------|----:|:------|
|died       |negative  | 3233|obama  |
|beloved    |positive  | 2467|obama  |
|loving     |positive  | 1514|obama  |
|peacefully |positive  | 1474|obama  |
|death      |negative  | 1294|obama  |
|love       |positive  | 1267|obama  |
|attacks    |negative  |  523|obama  |
|trump      |positive  | 3847|trump  |
|top        |positive  |  874|trump  |
|killed     |negative  |  726|trump  |
|fall       |negative  |  725|trump  |
|love       |positive  |  684|trump  |
|led        |positive  |  641|trump  |
Das ist aber unübersichtlich...

Die häufigsten Wörter der beiden Datensets vergleichen
========================================================

```r
## most common words
ggplot() +
  geom_bar(data = com_words,
           aes(x=word, y=n, fill = sentiment),
           stat="identity", position = "dodge", show.legend = FALSE) +
  facet_wrap(c("sentiment", "period"), scales = "free_y") +
  labs(y = "Häufigste Wörter nach Sentiment/Stimmung",
       x = NULL) +
  coord_flip() +
  scale_fill_manual(
    values = c(
      negative = "#c90000",
      positive = "#268302"
    )
  )
```
***
![plot of chunk unnamed-chunk-20](r_analysis-figure/unnamed-chunk-20-1.png)

Ergebnisse hinterfragen
========================================================
![plot of chunk unnamed-chunk-21](r_analysis-figure/unnamed-chunk-21-1.png)

Was fällt am Ergebnis auf?

Ergebnisse hinterfragen
========================================================
- Das Wort "Trump" muss als positives Wort aus dem Lexikon entfernt werden

```r
#remove word "trump" from as a positive sentiment
bing <- filter(bing, word != "trump")
```
- Coder erneut ausführen


Endergebnis
========================================================
![plot of chunk unnamed-chunk-24](r_analysis-figure/unnamed-chunk-24-1.png)

Endergebnis
========================================================
![plot of chunk unnamed-chunk-25](r_analysis-figure/unnamed-chunk-25-1.png)

Fazit: Was haben wir gelernt?
========================================================
- Was ist R und wie sieht die grundlegende Arbeitsweise in R aus?
  - Variabeln
  - Funktionen
  - Pakete
  - Grundzüge der Datentransformation, analysieren und visualisieren
  - Textanalyse mit dem Paket **tidytext**
- Einsatzmöglichkeit von digitalen Tools in der Textanalyse
- Die Lehrnkurve: Steil, aber der Aufwand kann schnell zum Ziel führen
- Die NYT schreibt positiver über Obama, als über Trump

Wie kann es weiter gehen?
========================================================
- Weitere Fragen an die Daten:
  - Zeitlicher Verlauf der Sentimente
  - Betrachtung anderer Zeiträume (z.B. Zeiten des Wahlkampfs)
  - Vergleich einzelner Artikelgruppen: z.B. Artikel die bestimmte Themen/Wörter enthalten
  - Analyse des Artikeltextes
  - Vergleich mit anderen Medien
  - Anwendung des Codes auf andere Datensets, z.B. Shakespeare
  
